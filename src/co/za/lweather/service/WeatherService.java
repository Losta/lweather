package co.za.lweather.service;

import java.io.IOException;
import java.util.Enumeration;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;

import co.za.lweather.business.Weather;
import co.za.lweather.business.WeatherBean;
import co.za.lweather.business.uti.WeatherUtil;
import co.za.lweather.dto.ClientWeatherTO;

/**
 * Servlet implementation class WeatherService
 */
@WebServlet("/lweather")
public class WeatherService extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private Weather weather;
	private WeatherUtil weatherUtil;
	
    /**
     * Default constructor. 
     */
    public WeatherService() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		String cityName = (String) request.getParameter("cityName");
		String countryCode = (String) request.getParameter("countryCode");
		ClientWeatherTO clientWeatherTO = getWeather().queryWeatherByCity(cityName, countryCode);
		Marshaller  marshaller = getWeatherUtil().prepXmlFile(clientWeatherTO);
		try {
			marshaller.marshal(clientWeatherTO,  response.getWriter());
			marshaller.marshal(clientWeatherTO, System.out);
		} catch (JAXBException e) {
			e.printStackTrace();
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	public Weather getWeather() {
		return new WeatherBean();
	}

	public void setWeather(Weather weather) {
		this.weather = weather;
	}

	public WeatherUtil getWeatherUtil() {
		return new WeatherUtil();
	}

	public void setWeatherUtil(WeatherUtil weatherUtil) {
		this.weatherUtil = weatherUtil;
	}

}
