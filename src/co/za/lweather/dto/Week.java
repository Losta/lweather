package co.za.lweather.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "week")
@XmlAccessorType (XmlAccessType.FIELD)
public class Week {
	
	private List<Day> day;

	public List<Day> getDay() {
		return day;
	}

	public void setDay(List<Day> clnDay) {
		this.day = clnDay;
	}
	
}
