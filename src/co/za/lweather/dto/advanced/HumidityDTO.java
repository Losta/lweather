package co.za.lweather.dto.advanced;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "humidity")
@XmlAccessorType (XmlAccessType.FIELD)
public class HumidityDTO {
	
	@XmlElement(name = "unit")
	private String value;
	
	@XmlElement(name = "unit")
	private String unit;

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

}
