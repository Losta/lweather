package co.za.lweather.dto.advanced;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Lonwabo The Dev
 *
 */


@XmlRootElement(name = "location")
@XmlAccessorType (XmlAccessType.FIELD)
public class LocationDTO {
	
	@XmlElement(name = "name")
	private String name;
	@XmlElement(name = "type")
	private String type;
	@XmlElement(name = "timeZone")
	private String timeZone;
	private String country;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getTimeZone() {
		return timeZone;
	}
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	
	
}
