package co.za.lweather.dto.advanced;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "windSpeed")
@XmlAccessorType (XmlAccessType.FIELD)
public class WindSpeedDTO {
	
	@XmlElement(name = "mps")
	private String mps;
	
	@XmlElement(name = "name")
	private String name;
	
	public String getMps() {
		return mps;
	}
	public void setMps(String mps) {
		this.mps = mps;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
