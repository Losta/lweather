package co.za.lweather.dto.advanced;

public class Location {

	private String altitude;
	private String latitude;
	private String longitude;
	private String geoBase;
	private String geobaseId;
	public String getAltitude() {
		return altitude;
	}
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	public String getLatitude() {
		return latitude;
	}
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	public String getLongitude() {
		return longitude;
	}
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	public String getGeoBase() {
		return geoBase;
	}
	public void setGeoBase(String geoBase) {
		this.geoBase = geoBase;
	}
	public String getGeobaseId() {
		return geobaseId;
	}
	public void setGeobaseId(String geobaseId) {
		this.geobaseId = geobaseId;
	}

}
