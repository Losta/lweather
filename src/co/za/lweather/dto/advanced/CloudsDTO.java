package co.za.lweather.dto.advanced;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "clouds")
@XmlAccessorType (XmlAccessType.FIELD)
public class CloudsDTO {

	@XmlElement(name = "value")
	private String value;
	
	@XmlElement(name = "all")
	private String all;
	
	@XmlElement(name = "unit")
	private String unit;
	
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	public String getAll() {
		return all;
	}
	public void setAll(String all) {
		this.all = all;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	
	
	
	
}
