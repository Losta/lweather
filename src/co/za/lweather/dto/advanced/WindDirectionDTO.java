package co.za.lweather.dto.advanced;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "windDirection")
@XmlAccessorType (XmlAccessType.FIELD)
public class WindDirectionDTO {
	
	@XmlElement(name = "deg")
	private String deg;
	
	@XmlElement(name = "code")
	private String code;
	
	@XmlElement(name = "name")
	private String name;
	
	public String getDeg() {
		return deg;
	}
	public void setDeg(String deg) {
		this.deg = deg;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
