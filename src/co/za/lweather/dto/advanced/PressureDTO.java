package co.za.lweather.dto.advanced;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "pressure")
@XmlAccessorType (XmlAccessType.FIELD)
public class PressureDTO {
	
	@XmlElement(name = "unit")
	 private String unit;
	
	@XmlElement(name = "value")
	 private String value;
	 
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	 
	 

}
