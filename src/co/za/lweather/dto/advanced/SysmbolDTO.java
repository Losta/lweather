package co.za.lweather.dto.advanced;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "symbol")
@XmlAccessorType (XmlAccessType.FIELD)
public class SysmbolDTO {
		
	@XmlElement(name = "number")
	private String number;
	
	@XmlElement(name = "name")
	private String name;
	
	@XmlElement(name = "var")
	private String var;
	
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getVar() {
		return var;
	}
	public void setVar(String var) {
		this.var = var;
	}
	
	
			
}
