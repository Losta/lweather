package co.za.lweather.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "forecast")
@XmlAccessorType (XmlAccessType.FIELD)
public class ForecastDTO {

	@XmlElement(name = "time")
	private List<TimeDTO> clnTime;

	public List<TimeDTO> getClnTime() {
		return clnTime;
	}

	public void setClnTime(List<TimeDTO> clnTime) {
		this.clnTime = clnTime;
	}
	
	
	
}
