package co.za.lweather.dto;

import java.util.List;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

import co.za.lweather.dto.advanced.LocationDTO;

@XmlRootElement(name = "clientWeatherTO")
@XmlAccessorType (XmlAccessType.FIELD)
public class ClientWeatherTO {
	
	private LocationDTO location;
	private double minAverageTemperature;
	private double maxAverageTemperature;
	private double minMaxAverageTemperature;
	private Week week;
	private List<Integer> unrecordedTemperature;
	private List<OverallMaxMinValueDTO> overallMaxMinCount;

	public Week getWeek() {
		return week;
	}

	public void setWeek(Week week) {
		this.week = week;
	}
	
	public LocationDTO getLocation() {
		return location;
	}

	public void setLocation(LocationDTO location) {
		this.location = location;
	}

	public double getMinAverageTemperature() {
		return minAverageTemperature;
	}

	public void setMinAverageTemperature(double minAverageTemperature) {
		this.minAverageTemperature = minAverageTemperature;
	}

	public double getMaxAverageTemperature() {
		return maxAverageTemperature;
	}

	public void setMaxAverageTemperature(double maxAverageTemperature) {
		this.maxAverageTemperature = maxAverageTemperature;
	}

	public double getMinMaxAverageTemperature() {
		return minMaxAverageTemperature;
	}

	public void setMinMaxAverageTemperature(double minMaxAverageTemperature) {
		this.minMaxAverageTemperature = minMaxAverageTemperature;
	}

	public void setOverallMaxMinCount(List<OverallMaxMinValueDTO> overallMaxMinCount) {
		this.overallMaxMinCount = overallMaxMinCount;
	}

	public List<Integer> getUnrecordedTemperature() {
		return unrecordedTemperature;
	}

	public void setUnrecordedTemperature(List<Integer> unrecordedTemperature) {
		this.unrecordedTemperature = unrecordedTemperature;
	}

	public List<OverallMaxMinValueDTO> getOverallMaxMinCount() {
		return overallMaxMinCount;
	}

	public void setOverallMaxMinCountCount(
			List<OverallMaxMinValueDTO> clnOverallMaxMinValueDTO) {
		this.overallMaxMinCount = clnOverallMaxMinValueDTO;
	}

	public LocationDTO getLocationDto() {
		return location;
	}

	public void setLocationDto(LocationDTO locationDto) {
		this.location = locationDto;
	}
	
	
	
}
