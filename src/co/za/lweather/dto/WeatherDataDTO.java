package co.za.lweather.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import co.za.lweather.dto.advanced.LocationDTO;

@XmlRootElement(name = "weatherdata")
@XmlAccessorType (XmlAccessType.FIELD)
public class WeatherDataDTO {

	@XmlElement(name = "location")
	LocationDTO locationDto;
	
	@XmlElement(name = "forecast")
	ForecastDTO forecastDTO;

	public LocationDTO getLocationDto() {
		return locationDto;
	}

	public void setLocationDto(LocationDTO locationDto) {
		this.locationDto = locationDto;
	}

	public ForecastDTO getForecastDTO() {
		return forecastDTO;
	}

	public void setForecastDTO(ForecastDTO forecastDTO) {
		this.forecastDTO = forecastDTO;
	}
	

}
