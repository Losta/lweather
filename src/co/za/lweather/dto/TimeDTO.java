package co.za.lweather.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "time")
@XmlAccessorType (XmlAccessType.FIELD)
public class TimeDTO {
	
	@XmlAttribute(name = "from")
    String from;
	
	@XmlAttribute(name = "to")
	 String to;
	
	@XmlElement(name = "temperature")
	private TemperatureDTO temperatureDTO;
	

	public String getFrom() {
		return from;
	}

	public void setFrom(String from) {
		this.from = from;
	}

	public String getTo() {
		return to;
	}

	public void setTo(String to) {
		this.to = to;
	}

	public TemperatureDTO getTemperatureDTO() {
		return temperatureDTO;
	}

	public void setTemperatureDTO(TemperatureDTO temperatureDTO) {
		this.temperatureDTO = temperatureDTO;
	}

}
