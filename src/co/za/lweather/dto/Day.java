package co.za.lweather.dto;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name = "day")
@XmlAccessorType (XmlAccessType.FIELD)
public class Day {
	
	private String dateOfDay;
	private int dayMinTemp;
	private int dayMaxTemp;
	
	public String getDateOfDay() {
		return dateOfDay;
	}
	public void setDateOfDay(String dateOfDay) {
		this.dateOfDay = dateOfDay;
	}
	public int getDayMinTemp() {
		return dayMinTemp;
	}
	public void setDayMinTemp(int dayMinTemp) {
		this.dayMinTemp = dayMinTemp;
	}
	public int getDayMaxTemp() {
		return dayMaxTemp;
	}
	public void setDayMaxTemp(int dayMaxTemp) {
		this.dayMaxTemp = dayMaxTemp;
	}
	
	

}
