package co.za.lweather.business;

import co.za.lweather.dto.ClientWeatherTO;

public interface Weather {
	
	public ClientWeatherTO queryWeatherByCity(String cityName, String countryCode);

}
