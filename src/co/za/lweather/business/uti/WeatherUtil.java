package co.za.lweather.business.uti;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;

import javax.servlet.http.HttpServletResponse;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;

public class WeatherUtil {

	public Marshaller prepXmlFile(Object object) {

		//JAXBContext jaxbContext = null;
		Marshaller jaxbMarshaller2 = null;

		try {
			JAXBContext  jaxbContext = JAXBContext.newInstance(object.getClass());
			Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
			jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
			jaxbMarshaller2 = jaxbMarshaller;

		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return jaxbMarshaller2;
	}

	public Unmarshaller readXmlFile(Object object) {

		JAXBContext jaxbContext;
		Unmarshaller jaxbUnmarshaller = null;
		try {
			jaxbContext = JAXBContext.newInstance(object.getClass());
			jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		} catch (JAXBException e) {
			e.printStackTrace();
		}

		return jaxbUnmarshaller;
	}

	public void writeToFile(String fileContent, String location) {

		BufferedWriter bw = null;
		FileWriter fw = null;

		try {
			java.util.Date dte = new java.util.Date();
			fw = new FileWriter("\\lweatherTemp\\temp" + new Date(dte.getTime())+ location+".xml");
			bw = new BufferedWriter(fw);
			bw.write(fileContent);

		} catch (IOException e) {

			e.printStackTrace();

		} finally {

			try {

				if (bw != null)
					bw.close();

				if (fw != null)
					fw.close();

			} catch (IOException ex) {

				ex.printStackTrace();
			}
		}

	}
	
	public URLConnection connectToApi(String urlString){
		 URL url = null;
		try {
			url = new URL(urlString);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	        String query = null;// = INSERT_HERE_YOUR_URL_PARAMETERS;

	        //make connection
	        URLConnection urlc = null;
			try {
				urlc = url.openConnection();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

	        //use post mode
	        urlc.setDoOutput(true);
	        urlc.setAllowUserInteraction(false);

	        //send query
	        PrintStream ps = null;
			try {
				ps = new PrintStream(urlc.getOutputStream());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	        ps.print(query);
	        ps.close();
	        
	        return urlc;

	}
	
	public String  getResults(URLConnection urlc){
		
		String result = "";
		
       //get result
       BufferedReader br = null;
		try {
			br = new BufferedReader(new InputStreamReader(urlc
			    .getInputStream()));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       String currentText = "";
       try {
			while ((currentText=br.readLine())!=null) {
				result = result +currentText;
			    
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
       try {
			br.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return result;
	}
	
	public File initializeFile(String fileName){
		
		File file = new File(fileName);
		return file;
	}

}
