package co.za.lweather.business;

import java.io.StringReader;
import java.net.URLConnection;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.xml.bind.JAXBException;

import co.za.lweather.business.uti.WeatherUtil;
import co.za.lweather.dto.ClientWeatherTO;
import co.za.lweather.dto.Day;
import co.za.lweather.dto.ForecastDTO;
import co.za.lweather.dto.OverallMaxMinValueDTO;
import co.za.lweather.dto.TemperatureDTO;
import co.za.lweather.dto.TimeDTO;
import co.za.lweather.dto.WeatherDataDTO;
import co.za.lweather.dto.Week;


public class WeatherBean implements Weather {

	private  String WEATHER_URL = "http://api.openweathermap.org/data/2.5/forecast?mode=xml&units=metric&APPID=853cd505b15db7257142d46806b19a3e&q=";
	private WeatherUtil weatherUtil;
	
	@Override
	public ClientWeatherTO queryWeatherByCity(String cityName, String countryCode) {
		
		countryCode = initStringValue(countryCode); //check if the value is not null
		WEATHER_URL = WEATHER_URL+cityName+","+countryCode;
		URLConnection urlc = getWeatherUtil().connectToApi(WEATHER_URL);
		String result = getWeatherUtil().getResults(urlc);
		WeatherDataDTO weatherDataDTO = new WeatherDataDTO();
		ClientWeatherTO clientWeatherTO = null;
		try {
			StringReader reader = new StringReader(result); // test
			weatherDataDTO= (WeatherDataDTO) getWeatherUtil().readXmlFile(weatherDataDTO).unmarshal(reader);
			clientWeatherTO = processWeatherMinMaxTemperatureFromApi(weatherDataDTO);//Extracting the minimum and maximum daily temperatures
			clientWeatherTO = processMinAndMaxAverageTemperature(clientWeatherTO);//Extracting the minimum and maximum Average temperatures
			clientWeatherTO = processMissingTemperatures(clientWeatherTO); //Extracting the minimum and maximum daily temperature processing temperatures that are not appearing for the week
			clientWeatherTO = processTemperaturesOccurances(clientWeatherTO); // Generating the keys to see how many a temperature appears a week
			clientWeatherTO.setLocationDto(weatherDataDTO.getLocationDto()); // setting the requested location
		} catch (JAXBException e) {
			e.printStackTrace();
		}
		return clientWeatherTO;
	}
	
	//a)Question 1a
	private ClientWeatherTO processWeatherMinMaxTemperatureFromApi(WeatherDataDTO weatherDataDTO){
		
		ForecastDTO forecastDTO = weatherDataDTO.getForecastDTO(); // the forcast object contains all the temperature data
		List<TimeDTO> clnTime = forecastDTO.getClnTime(); // This list contains the temperature results for different times of the day
		clnTime.remove(0);
		String tempTimeValue;
		Day day= null; // To keep temperatures of the day
		Week week = new Week(); //the week object will hold the days of the week
		week.setDay(new ArrayList<Day>());
		int counter =-1;
		ClientWeatherTO clientWeatherTO = new ClientWeatherTO();
		for(TimeDTO timeDTO:clnTime){ // Iterating to get the temperature for each day and add it to the week object
			
			TemperatureDTO temperatureDTO = timeDTO.getTemperatureDTO();
			tempTimeValue = timeDTO.getFrom().substring(0, 10);
			double temperature = Double.parseDouble(temperatureDTO.getValue());
			if(day==null || (day.getDateOfDay()!=null && !day.getDateOfDay().equalsIgnoreCase(tempTimeValue))){
				day = new Day();
				day.setDateOfDay(tempTimeValue);
				day.setDayMaxTemp((int)temperature);
				day.setDayMinTemp((int)temperature);
				week.getDay().add(day);
				counter++;
			}else{
				day = week.getDay().get(counter);
				if(temperature<day.getDayMinTemp()){ //because the Api returns the temperature of the day as time slots, I do this check to see what is the minimum and maximum value of that day
					week.getDay().get(counter).setDayMinTemp((int)temperature);
				}else if(temperature>day.getDayMaxTemp()){
					week.getDay().get(counter).setDayMaxTemp((int)temperature);
				}
				
			}
		}
		clientWeatherTO.setWeek(week);
		return clientWeatherTO;
	}
	
	//Question 1b)
	private ClientWeatherTO processMinAndMaxAverageTemperature(ClientWeatherTO clientWeatherTO){
		
		double totOfBoth = 0;
		Week week = clientWeatherTO.getWeek();
		List<Day> clnDay = week.getDay();
		int counter=0;
		double tempMinTot=0;
		double tempMaxTot=0;
		for(Day day:clnDay){
			
			//Accumulating the total of all max and mim temperature returned from the API
				
				tempMinTot = doAddition(tempMinTot,day.getDayMinTemp());
				tempMaxTot = doAddition(tempMaxTot,day.getDayMaxTemp());
				totOfBoth = tempMinTot + tempMinTot;
				totOfBoth = tempMinTot + tempMaxTot;
				
			counter++;
			
		}
		
		// calculating the average result of min, max and the combined min and max average
		double tempMinMaxTot = totOfBoth/counter;
		tempMinTot = tempMinTot/counter;
		tempMaxTot = tempMaxTot/counter;
		counter = counter * 2;
		clientWeatherTO.setMaxAverageTemperature(tempMaxTot);
		clientWeatherTO.setMinAverageTemperature(tempMinTot);
		clientWeatherTO.setMinMaxAverageTemperature(tempMinMaxTot);
		
		return clientWeatherTO;
	}
	
	//question 1c
	private ClientWeatherTO processMissingTemperatures(ClientWeatherTO clientWeatherTO){
		
		//checking the temperatures that are not in the current week being process
		Integer temperature[] = initTemperatureArray(clientWeatherTO); // this is an array that is going to add all the temperatures
		int minTemp;
		int maxTemp;
		//sorting the list from low to high value so that i can tell which temperatures are not part of this week temperatures
		for(int i=0;i<temperature.length;i++){
			
			for(int n=0;n<temperature.length;n++){
				
				int switchValue;
				if(temperature[n]>temperature[i]){
					
					switchValue = temperature[i];
					temperature[i] =  temperature[n]; 
				    temperature[n] = switchValue;	    
				}
			}
		}

		minTemp = temperature[0]; // value that is going to keep my minimum temperature, this will be ny starting point when checking the missing temperatures
		maxTemp = temperature[temperature.length-1];// value that is going to keep my maximum temperature, this will be my end point when checking the missing temperatures
		
		ArrayList<Integer> list =  new ArrayList<Integer>(); 
		list.addAll(Arrays.asList(temperature));
		clientWeatherTO.setUnrecordedTemperature(new ArrayList<Integer>());
		//iterating the array that contauns the temperatures to see which temperature is missing and add it to the container that is going to keep all the missing temperatures
		for(int i=minTemp;i<=maxTemp;i++){
			if(!list.contains(new Integer(i))){
				clientWeatherTO.getUnrecordedTemperature().add(i);
			}
		}
		
		return clientWeatherTO;
	}
	
	//Question 1d & 1e
	private ClientWeatherTO processTemperaturesOccurances(ClientWeatherTO clientWeatherTO){
		//checking how many times a temperature appears in week
		Integer temperature[] = initTemperatureArray(clientWeatherTO);
		Map<Integer, Integer> mpTemp = new HashMap<Integer, Integer>(); // this map is going to keep the temperature, the key will be the temperature and the value will be the counter

		for(int i=0;i<temperature.length;i++){
			
				if(mpTemp.containsKey(temperature[i])){
					int value = mpTemp.get(temperature[i]);
					value = value + 1;
					mpTemp.put(temperature[i], value);
				}else{
					mpTemp.put(temperature[i], 1);
				}
			}
		
		clientWeatherTO.setOverallMaxMinCountCount(new ArrayList<OverallMaxMinValueDTO>());
		OverallMaxMinValueDTO overallMaxMinValueDTO;
		
		for (Map.Entry<Integer, Integer> entry : mpTemp.entrySet()) {
			// adding the final result to the object that I am going to be sending to the client
			overallMaxMinValueDTO = new OverallMaxMinValueDTO();
			overallMaxMinValueDTO.setKey(entry.getKey().toString());
			overallMaxMinValueDTO.setValue(entry.getValue());
			clientWeatherTO.getOverallMaxMinCount().add(overallMaxMinValueDTO);

			
		}

		return clientWeatherTO;
	}
	
	private Integer[] initTemperatureArray(ClientWeatherTO clientWeatherTO){
		
		Week week = clientWeatherTO.getWeek();
		List<Day> clnDay = week.getDay();
		int arraySize = clnDay.size() * 2;
		Integer temperature[]=new Integer[arraySize];
		int index= 0;
		for(Day day:clnDay){
			temperature[index] = day.getDayMaxTemp();
			index++;
			temperature[index] = day.getDayMinTemp();
			index++;
		}
		
		return temperature;
		
	}
	
	private double doAddition(double value1, double value2){
			
		return value1+value2;
	}
	
	private String initStringValue(String value){
		return value==null?"":value;
	}
	
	public WeatherUtil getWeatherUtil() {
		return new WeatherUtil();
	}
	
	// this is a mock data returned by the api
	private String getTestXml(){
		return "<weatherdata>"
				+"<location>"
				+"<name>Cape Town</name>"
				+"<type/>"
				+"<country>ZA</country>"
				+"<timezone/>"
				+"<location altitude='0' latitude='-33.9259' longitude='18.4232' geobase='geonames' geobaseid='3369157'/>"
				+"</location>"
				+"<credit/>"
				+"<meta>"
				+"<lastupdate/>"
				+"<calctime>0.0037</calctime>"
				+"<nextupdate/>"
				+"</meta>"
				+"<sun rise='2017-05-18T05:34:14' set='2017-05-18T15:51:02'/>"
				+"<forecast>"
				+"<time from='2017-05-18T15:00:00' to='2017-05-18T18:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='152.507' code='SSE' name='South-southeast'/>"
				+"<windSpeed mps='3.26' name='Light breeze'/>"
				+"<temperature unit='celsius' value='13.01' min='13.01' max='13.01'/>"
				+"<pressure unit='hPa' value='1025.98'/>"
				+"<humidity value='97' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-18T18:00:00' to='2017-05-18T21:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='134' code='SE' name='SouthEast'/>"
				+"<windSpeed mps='2.5' name='Light breeze'/>"
				+"<temperature unit='celsius' value='11.96' min='11.96' max='11.96'/>"
				+"<pressure unit='hPa' value='1026.58'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-18T21:00:00' to='2017-05-19T00:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='127.002' code='SE' name='SouthEast'/>"
				+"<windSpeed mps='2.07' name='Light breeze'/>"
				+"<temperature unit='celsius' value='11.34' min='11.34' max='11.34'/>"
				+"<pressure unit='hPa' value='1026.59'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-19T00:00:00' to='2017-05-19T03:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='128.502' code='SE' name='SouthEast'/>"
				+"<windSpeed mps='1.67' name='Light breeze'/>"
				+"<temperature unit='celsius' value='11.09' min='11.09' max='11.09'/>"
				+"<pressure unit='hPa' value='1026.42'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-19T03:00:00' to='2017-05-19T06:00:00'>"
				+"<symbol number='800' name='clear sky' var='01d'/>"
				+"<precipitation/>"
				+"<windDirection deg='123.503' code='ESE' name='East-southeast'/>"
				+"<windSpeed mps='1.36' name='Calm'/>"
				+"<temperature unit='celsius' value='11.13' min='11.13' max='11.13'/>"
				+"<pressure unit='hPa' value='1026.99'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-19T06:00:00' to='2017-05-19T09:00:00'>"
				+"<symbol number='800' name='clear sky' var='01d'/>"
				+"<precipitation/>"
				+"<windDirection deg='172.501' code='S' name='South'/>"
				+"<windSpeed mps='0.77' name='Calm'/>"
				+"<temperature unit='celsius' value='14.77' min='14.77' max='14.77'/>"
				+"<pressure unit='hPa' value='1028.03'/>"
				+"<humidity value='89' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-19T09:00:00' to='2017-05-19T12:00:00'>"
				+"<symbol number='800' name='clear sky' var='01d'/>"
				+"<precipitation/>"
				+"<windDirection deg='216.503' code='SW' name='Southwest'/>"
				+"<windSpeed mps='1.61' name='Light breeze'/>"
				+"<temperature unit='celsius' value='15.79' min='15.79' max='15.79'/>"
				+"<pressure unit='hPa' value='1027.17'/>"
				+"<humidity value='82' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-19T12:00:00' to='2017-05-19T15:00:00'>"
				+"<symbol number='800' name='clear sky' var='01d'/>"
				+"<precipitation/>"
				+"<windDirection deg='208.502' code='SSW' name='South-southwest'/>"
				+"<windSpeed mps='1.63' name='Light breeze'/>"
				+"<temperature unit='celsius' value='15.16' min='15.16' max='15.16'/>"
				+"<pressure unit='hPa' value='1027.67'/>"
				+"<humidity value='87' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-19T15:00:00' to='2017-05-19T18:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='202.5' code='SSW' name='South-southwest'/>"
				+"<windSpeed mps='1.57' name=''/>"
				+"<temperature unit='celsius' value='12.77' min='12.77' max='12.77'/>"
				+"<pressure unit='hPa' value='1029'/>"
				+"<humidity value='96' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-19T18:00:00' to='2017-05-19T21:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='144.501' code='SE' name='SouthEast'/>"
				+"<windSpeed mps='0.92' name='Calm'/>"
				+"<temperature unit='celsius' value='11.9' min='11.9' max='11.9'/>"
				+"<pressure unit='hPa' value='1029.87'/>"
				+"<humidity value='98' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-19T21:00:00' to='2017-05-20T00:00:00'>"
				+"<symbol number='500' name='light rain' var='10n'/>"
				+"<precipitation unit='3h' value='0.005' type='rain'/>"
				+"<windDirection deg='71.0009' code='ENE' name='East-northeast'/>"
				+"<windSpeed mps='1.06' name='Calm'/>"
				+"<temperature unit='celsius' value='11.07' min='11.07' max='11.07'/>"
				+"<pressure unit='hPa' value='1029.45'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='few clouds' all='20' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-20T00:00:00' to='2017-05-20T03:00:00'>"
				+"<symbol number='500' name='light rain' var='10n'/>"
				+"<precipitation unit='3h' value='0.04' type='rain'/>"
				+"<windDirection deg='51.0008' code='NE' name='NorthEast'/>"
				+"<windSpeed mps='1.31' name='Calm'/>"
				+"<temperature unit='celsius' value='10.48' min='10.48' max='10.48'/>"
				+"<pressure unit='hPa' value='1029.19'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='scattered clouds' all='48' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-20T03:00:00' to='2017-05-20T06:00:00'>"
				+"<symbol number='500' name='light rain' var='10d'/>"
				+"<precipitation unit='3h' value='0.11' type='rain'/>"
				+"<windDirection deg='52.5018' code='NE' name='NorthEast'/>"
				+"<windSpeed mps='1.21' name='Calm'/>"
				+"<temperature unit='celsius' value='11.61' min='11.61' max='11.61'/>"
				+"<pressure unit='hPa' value='1030.08'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='overcast clouds' all='92' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-20T06:00:00' to='2017-05-20T09:00:00'>"
				+"<symbol number='500' name='light rain' var='10d'/>"
				+"<precipitation unit='3h' value='0.08' type='rain'/>"
				+"<windDirection deg='72.5005' code='ENE' name='East-northeast'/>"
				+"<windSpeed mps='1.52' name=''/>"
				+"<temperature unit='celsius' value='13.69' min='13.69' max='13.69'/>"
				+"<pressure unit='hPa' value='1030.52'/>"
				+"<humidity value='97' unit='%'/>"
				+"<clouds value='few clouds' all='24' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-20T09:00:00' to='2017-05-20T12:00:00'>"
				+"<symbol number='800' name='clear sky' var='01d'/>"
				+"<precipitation/>"
				+"<windDirection deg='218.004' code='SW' name='Southwest'/>"
				+"<windSpeed mps='1.75' name='Light breeze'/>"
				+"<temperature unit='celsius' value='15.27' min='15.27' max='15.27'/>"
				+"<pressure unit='hPa' value='1029.41'/>"
				+"<humidity value='85' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-20T12:00:00' to='2017-05-20T15:00:00'>"
				+"<symbol number='800' name='clear sky' var='01d'/>"
				+"<precipitation/>"
				+"<windDirection deg='190.501' code='S' name='South'/>"
				+"<windSpeed mps='1.34' name='Calm'/>"
				+"<temperature unit='celsius' value='14.82' min='14.82' max='14.82'/>"
				+"<pressure unit='hPa' value='1029.41'/>"
				+"<humidity value='87' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-20T15:00:00' to='2017-05-20T18:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='167.501' code='SSE' name='South-southeast'/>"
				+"<windSpeed mps='1.06' name='Calm'/>"
				+"<temperature unit='celsius' value='12.41' min='12.41' max='12.41'/>"
				+"<pressure unit='hPa' value='1029.58'/>"
				+"<humidity value='97' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-20T18:00:00' to='2017-05-20T21:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='148.001' code='SSE' name='South-southeast'/>"
				+"<windSpeed mps='1.12' name='Calm'/>"
				+"<temperature unit='celsius' value='11.59' min='11.59' max='11.59'/>"
				+"<pressure unit='hPa' value='1029.77'/>"
				+"<humidity value='99' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-20T21:00:00' to='2017-05-21T00:00:00'>"
				+"<symbol number='800' name='clear sky' var='01n'/>"
				+"<precipitation/>"
				+"<windDirection deg='116' code='ESE' name='East-southeast'/>"
				+"<windSpeed mps='1.31' name='Calm'/>"
				+"<temperature unit='celsius' value='11.25' min='11.25' max='11.25'/>"
				+"<pressure unit='hPa' value='1029.22'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-21T00:00:00' to='2017-05-21T03:00:00'>"
				+"<symbol number='500' name='light rain' var='10n'/>"
				+"<precipitation unit='3h' value='0.005' type='rain'/>"
				+"<windDirection deg='113.006' code='ESE' name='East-southeast'/>"
				+"<windSpeed mps='1.76' name='Light breeze'/>"
				+"<temperature unit='celsius' value='10.71' min='10.71' max='10.71'/>"
				+"<pressure unit='hPa' value='1028.53'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='scattered clouds' all='32' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-21T03:00:00' to='2017-05-21T06:00:00'>"
				+"<symbol number='500' name='light rain' var='10d'/>"
				+"<precipitation unit='3h' value='0.03' type='rain'/>"
				+"<windDirection deg='114.503' code='ESE' name='East-southeast'/>"
				+"<windSpeed mps='2.01' name='Light breeze'/>"
				+"<temperature unit='celsius' value='9.99' min='9.99' max='9.99'/>"
				+"<pressure unit='hPa' value='1028.76'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='clear sky' all='8' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-21T06:00:00' to='2017-05-21T09:00:00'>"
				+"<symbol number='500' name='light rain' var='10d'/>"
				+"<precipitation unit='3h' value='0.01' type='rain'/>"
				+"<windDirection deg='74.5014' code='ENE' name='East-northeast'/>"
				+"<windSpeed mps='1.65' name='Light breeze'/>"
				+"<temperature unit='celsius' value='13.52' min='13.52' max='13.52'/>"
				+"<pressure unit='hPa' value='1028.38'/>"
				+"<humidity value='97' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-21T09:00:00' to='2017-05-21T12:00:00'>"
				+"<symbol number='801' name='few clouds' var='02d'/>"
				+"<precipitation/>"
				+"<windDirection deg='189.007' code='S' name='South'/>"
				+"<windSpeed mps='0.97' name='Calm'/>"
				+"<temperature unit='celsius' value='15.3' min='15.3' max='15.3'/>"
				+"<pressure unit='hPa' value='1026.47'/>"
				+"<humidity value='83' unit='%'/>"
				+"<clouds value='few clouds' all='12' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-21T12:00:00' to='2017-05-21T15:00:00'>"
				+"<symbol number='800' name='clear sky' var='01d'/>"
				+"<precipitation/>"
				+"<windDirection deg='224.504' code='SW' name='Southwest'/>"
				+"<windSpeed mps='1.86' name='Light breeze'/>"
				+"<temperature unit='celsius' value='15.7' min='15.7' max='15.7'/>"
				+"<pressure unit='hPa' value='1025.48'/>"
				+"<humidity value='79' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-21T15:00:00' to='2017-05-21T18:00:00'>"
				+"<symbol number='800' name='clear sky' var='02n'/>"
				+"<precipitation/>"
				+"<windDirection deg='171.004' code='S' name='South'/>"
				+"<windSpeed mps='1.3' name='Calm'/>"
				+"<temperature unit='celsius' value='12.26' min='12.26' max='12.26'/>"
				+"<pressure unit='hPa' value='1026.07'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='clear sky' all='8' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-21T18:00:00' to='2017-05-21T21:00:00'>"
				+"<symbol number='801' name='few clouds' var='02n'/>"
				+"<precipitation/>"
				+"<windDirection deg='94.5028' code='E' name='East'/>"
				+"<windSpeed mps='0.61' name='Calm'/>"
				+"<temperature unit='celsius' value='11.29' min='11.29' max='11.29'/>"
				+"<pressure unit='hPa' value='1026.47'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='few clouds' all='20' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-21T21:00:00' to='2017-05-22T00:00:00'>"
				+"<symbol number='801' name='few clouds' var='02n'/>"
				+"<precipitation/>"
				+"<windDirection deg='256.011' code='WSW' name='West-southwest'/>"
				+"<windSpeed mps='0.72' name='Calm'/>"
				+"<temperature unit='celsius' value='11.01' min='11.01' max='11.01'/>"
				+"<pressure unit='hPa' value='1026.23'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='few clouds' all='24' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-22T00:00:00' to='2017-05-22T03:00:00'>"
				+"<symbol number='500' name='light rain' var='10n'/>"
				+"<precipitation unit='3h' value='0.01' type='rain'/>"
				+"<windDirection deg='324.506' code='NW' name='Northwest'/>"
				+"<windSpeed mps='0.97' name='Calm'/>"
				+"<temperature unit='celsius' value='11' min='11' max='11'/>"
				+"<pressure unit='hPa' value='1025.67'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='few clouds' all='24' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-22T03:00:00' to='2017-05-22T06:00:00'>"
				+"<symbol number='500' name='light rain' var='10d'/>"
				+"<precipitation unit='3h' value='0.05' type='rain'/>"
				+"<windDirection deg='243.003' code='WSW' name='West-southwest'/>"
				+"<windSpeed mps='0.81' name='Calm'/>"
				+"<temperature unit='celsius' value='11.35' min='11.35' max='11.35'/>"
				+"<pressure unit='hPa' value='1027.19'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='scattered clouds' all='44' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-22T06:00:00' to='2017-05-22T09:00:00'>"
				+"<symbol number='500' name='light rain' var='10d'/>"
				+"<precipitation unit='3h' value='0.06' type='rain'/>"
				+"<windDirection deg='91.0034' code='E' name='East'/>"
				+"<windSpeed mps='0.56' name='Calm'/>"
				+"<temperature unit='celsius' value='13.74' min='13.74' max='13.74'/>"
				+"<pressure unit='hPa' value='1028.27'/>"
				+"<humidity value='97' unit='%'/>"
				+"<clouds value='overcast clouds' all='88' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-22T09:00:00' to='2017-05-22T12:00:00'>"
				+"<symbol number='500' name='light rain' var='10d'/>"
				+"<precipitation unit='3h' value='0.01' type='rain'/>"
				+"<windDirection deg='162.503' code='SSE' name='South-southeast'/>"
				+"<windSpeed mps='1.04' name='Calm'/>"
				+"<temperature unit='celsius' value='15.05' min='15.05' max='15.05'/>"
				+"<pressure unit='hPa' value='1027.04'/>"
				+"<humidity value='88' unit='%'/>"
				+"<clouds value='clear sky' all='8' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-22T12:00:00' to='2017-05-22T15:00:00'>"
				+"<symbol number='802' name='scattered clouds' var='03d'/>"
				+"<precipitation/>"
				+"<windDirection deg='154.501' code='SSE' name='South-southeast'/>"
				+"<windSpeed mps='1.83' name='Light breeze'/>"
				+"<temperature unit='celsius' value='15.24' min='15.24' max='15.24'/>"
				+"<pressure unit='hPa' value='1026.49'/>"
				+"<humidity value='87' unit='%'/>"
				+"<clouds value='scattered clouds' all='32' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-22T15:00:00' to='2017-05-22T18:00:00'>"
				+"<symbol number='802' name='scattered clouds' var='03n'/>"
				+"<precipitation/>"
				+"<windDirection deg='135.501' code='SE' name='SouthEast'/>"
				+"<windSpeed mps='3.02' name='Light breeze'/>"
				+"<temperature unit='celsius' value='13.63' min='13.63' max='13.63'/>"
				+"<pressure unit='hPa' value='1027.07'/>"
				+"<humidity value='93' unit='%'/>"
				+"<clouds value='scattered clouds' all='44' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-22T18:00:00' to='2017-05-22T21:00:00'>"
				+"<symbol number='801' name='few clouds' var='02n'/>"
				+"<precipitation/>"
				+"<windDirection deg='120.502' code='ESE' name='East-southeast'/>"
				+"<windSpeed mps='3.15' name='Light breeze'/>"
				+"<temperature unit='celsius' value='13.31' min='13.31' max='13.31'/>"
				+"<pressure unit='hPa' value='1026.99'/>"
				+"<humidity value='92' unit='%'/>"
				+"<clouds value='few clouds' all='20' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-22T21:00:00' to='2017-05-23T00:00:00'>"
				+"<symbol number='802' name='scattered clouds' var='03n'/>"
				+"<precipitation/>"
				+"<windDirection deg='103.001' code='ESE' name='East-southeast'/>"
				+"<windSpeed mps='2.7' name='Light breeze'/>"
				+"<temperature unit='celsius' value='12.64' min='12.64' max='12.64'/>"
				+"<pressure unit='hPa' value='1026.27'/>"
				+"<humidity value='95' unit='%'/>"
				+"<clouds value='scattered clouds' all='44' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-23T00:00:00' to='2017-05-23T03:00:00'>"
				+"<symbol number='801' name='few clouds' var='02n'/>"
				+"<precipitation/>"
				+"<windDirection deg='81.5027' code='E' name='East'/>"
				+"<windSpeed mps='2.73' name='Light breeze'/>"
				+"<temperature unit='celsius' value='11.66' min='11.66' max='11.66'/>"
				+"<pressure unit='hPa' value='1025.12'/>"
				+"<humidity value='100' unit='%'/>"
				+"<clouds value='few clouds' all='24' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-23T03:00:00' to='2017-05-23T06:00:00'>"
				+"<symbol number='800' name='clear sky' var='01d'/>"
				+"<precipitation/>"
				+"<windDirection deg='66.5063' code='ENE' name='East-northeast'/>"
				+"<windSpeed mps='2.33' name='Light breeze'/>"
				+"<temperature unit='celsius' value='11.64' min='11.64' max='11.64'/>"
				+"<pressure unit='hPa' value='1024.98'/>"
				+"<humidity value='97' unit='%'/>"
				+"<clouds value='clear sky' all='0' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-23T06:00:00' to='2017-05-23T09:00:00'>"
				+"<symbol number='800' name='clear sky' var='02d'/>"
				+"<precipitation/>"
				+"<windDirection deg='27.5028' code='NNE' name='North-northeast'/>"
				+"<windSpeed mps='1.61' name='Light breeze'/>"
				+"<temperature unit='celsius' value='15.91' min='15.91' max='15.91'/>"
				+"<pressure unit='hPa' value='1025.86'/>"
				+"<humidity value='77' unit='%'/>"
				+"<clouds value='clear sky' all='8' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-23T09:00:00' to='2017-05-23T12:00:00'>"
				+"<symbol number='801' name='few clouds' var='02d'/>"
				+"<precipitation/>"
				+"<windDirection deg='353.501' code='' name=''/>"
				+"<windSpeed mps='1.39' name='Calm'/>"
				+"<temperature unit='celsius' value='19.44' min='19.44' max='19.44'/>"
				+"<pressure unit='hPa' value='1024.3'/>"
				+"<humidity value='61' unit='%'/>"
				+"<clouds value='few clouds' all='24' unit='%'/>"
				+"</time>"
				+"<time from='2017-05-23T12:00:00' to='2017-05-23T15:00:00'>"
				+"<symbol number='803' name='broken clouds' var='04d'/>"
				+"<precipitation/>"
				+"<windDirection deg='324.5' code='NW' name='Northwest'/>"
				+"<windSpeed mps='0.78' name='Calm'/>"
				+"<temperature unit='celsius' value='18.89' min='18.89' max='18.89'/>"
				+"<pressure unit='hPa' value='1023.91'/>"
				+"<humidity value='63' unit='%'/>"
				+"<clouds value='broken clouds' all='56' unit='%'/>"
				+"</time>"
				+"</forecast>"
				+"</weatherdata>";
	}

}
