package co.za.lweather.test;

import java.io.File;

import javax.xml.bind.JAXBException;

import co.za.lweather.business.Weather;
import co.za.lweather.business.WeatherBean;
import co.za.lweather.business.uti.WeatherUtil;
import co.za.lweather.dto.WeatherDataDTO;

public class Tester {

	WeatherUtil weatherUtil;
	Weather weather;
	
	 public static void main(String[]args){

		 new Tester().testIntergration();
		 System.out.println("done");
		 
	 }
	 
	 public void testIntergration(){
		 weather= new WeatherBean();
		 weather.queryWeatherByCity("East London", "za");
	 }
	 
	 public void testApiConnection(){
		 
		 weather= new WeatherBean();
		 weather.queryWeatherByCity("", "za");
		 
	 }
	 
	 public void testMarshallingFromXmlToObject(){
		 
		 
		 WeatherDataDTO weatherDataDTO = new WeatherDataDTO();
		 weatherUtil = new WeatherUtil();
		 //weatherUtil.writeXmlFile(object, fileName);
		try {
			weatherDataDTO= (WeatherDataDTO) weatherUtil.readXmlFile(weatherUtil).unmarshal(new File(""));
		} catch (JAXBException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	 }
	 
	 public void testWriteToFile(){
		 
		 weatherUtil = new WeatherUtil();
		 weatherUtil.writeToFile("this is just a test wena Losta wenzani ", "Cape town");

	 }
	
}
