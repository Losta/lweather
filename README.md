Tools:

java
Servlet-api.jar
Ant
Tomcat
eclipes

How To Run the project

(option 1)

-Copy the war file from the lweather  project under the path ==> lweather\deploy\lweather.war
		or build a new one by running the build.ant script, the script is onthe root folder of the project
-Copy the war file from the lweather project under the path ==> lweather\deploy\lweather.war 
-Copy the file to  tomcat under deploy folder found in the projec and place it inside the webapps folder
-Start the tomcat server
-Open the browser and go to http://localhost:8080/lweather

(option 2, using eclipes)

-Import the project to your eclipes
-Add the jar Servlet-api.jar to your build path(you can find this library  the bin folder of tomcat or download it)
-Add a server under the server tab on your eclipes
-Rigth click the project and select run on the server
-Open the browser and go to http://localhost:8080/lweather






