function queryWeather(){
	
	var cityName = $('#cityName').val();
	var countryCode = $('#countryCode').val();
    $.ajax({
    	    url:'lweather',
	        data:{cityName:cityName,countryCode:countryCode},
	        type:'get',
	        contentType: false,
	        cache:false,
	        success:function(data){
	        	readServerData(data);
	        },
		        error:function(){
		          alert('error');
	        }
     }
);
	
}

function readServerData(serverData){
	
	
	var xmlDoc = $.parseXML(serverData);
	var	 node = xmlDoc.getElementsByTagName('clientWeatherTO')[0]; 
	var location = node.getElementsByTagName('location')[0];
	
	var locationName  = location.getElementsByTagName("name")[0];
	var locationCountryCode  = location.getElementsByTagName("country")[0];

	locationName = prepValue(locationName);
	locationCountryCode = prepValue(locationCountryCode);
	
	

	
	//$('#weatheHeader').text("Weather Update("+locationName+" ,"+locationCountryCode+")");
	var week = readData(serverData, 'clientWeatherTO', 'week');
	var days = week.getElementsByTagName('day');
	$('#5dayMinMaxWthRdg') .children().remove();
	var header ="<tr><th><font size='2'>Date<font></th><th><font size='2'>Minimum Temperature<font></th><th><font size='2'>Maximum Temperature<font></th></tr>";
	$('#5dayMinMaxWthRdg').append(header);
	for(var i=0;i<days.length;i++){
			var day = days[i];
			var dateOfDay  = day.getElementsByTagName("dateOfDay")[0];
			var dayMinTemp  = day.getElementsByTagName("dayMinTemp")[0];
			var dayMaxTemp  = day.getElementsByTagName("dayMaxTemp")[0];
			
			dateOfDay = prepValue(dateOfDay);
			dayMinTemp = prepValue(dayMinTemp);
			dayMaxTemp = prepValue(dayMaxTemp);
			if(i>0){
				var rowData ="<tr><td>"+dateOfDay+"</td><td>"+dayMinTemp+"</td><td>"+dayMaxTemp+"</td></tr>";
				$('#5dayMinMaxWthRdg').append(rowData);
			}else{
				$('#weatheHeader').text("Weather Update("+locationName+" ,"+locationCountryCode+")Current Temperature ="+dayMaxTemp);
			}

	}
	var minAverageTemperature = readData(serverData, 'clientWeatherTO', 'minAverageTemperature');
	var maxAverageTemperature = readData(serverData, 'clientWeatherTO', 'maxAverageTemperature');
	var minMaxAverageTemperature = readData(serverData, 'clientWeatherTO', 'minMaxAverageTemperature');
	
	minAverageTemperature = prepValue(minAverageTemperature);
	maxAverageTemperature = prepValue(maxAverageTemperature);
	minMaxAverageTemperature = prepValue(minMaxAverageTemperature);
	
	$('#5dayTmpAvrRdn') .children().remove();
	var header ="<tr><th><font size='2'>Average Maximum Temperature<font></th><th><font size='2'>Average Minimum Temperature<font></th><th><font size='2'>	Average Minimum and Maximum Temperature<font></th></tr>";
	$('#5dayTmpAvrRdn').append(header);
	var rowData ="<tr><td>"+maxAverageTemperature+"</td><td>"+minAverageTemperature+"</td><td>"+minMaxAverageTemperature+"</td></tr>";
	$('#5dayTmpAvrRdn').append(rowData);
	
	var xmlDoc = $.parseXML(serverData);
	var	 node = xmlDoc.getElementsByTagName('clientWeatherTO')[0]; 
	var overallMinMaxOccurances = node.getElementsByTagName('overallMaxMinCount');
	$('#5dayOccCount') .children().remove();
	var header ="<tr><th><font size='2'>Temperature<font></th><th><font size='2'>Quantity</th></tr>";
	$('#5dayOccCount').append(header);
	for(var i=0;i<overallMinMaxOccurances.length;i++){
		
		var temperature = overallMinMaxOccurances[i];
		var temperatureKey  = temperature.getElementsByTagName("key")[0];
		var temperatureValue  = temperature.getElementsByTagName("value")[0];
		
		
		temperatureKey = prepValue(temperatureKey);
		temperatureValue = prepValue(temperatureValue);

		var rowData ="<tr><td>"+temperatureKey+"</td><td>"+temperatureValue+"</td></tr>";
		$('#5dayOccCount').append(rowData);
	}
	
	var unrecordedTemperature = node.getElementsByTagName('unrecordedTemperature');
	$('#unrecordedTemp') .children().remove();
	var header ="<tr><th><font size='2'>Temperature<font></th></tr>";
	$('#unrecordedTemp').append(header);
	for(var i=0;i<unrecordedTemperature.length;i++){
		
		var temperature = unrecordedTemperature[i];
		temperature = prepValue(temperature);
		var rowData ="<tr><td>"+temperature+"</td></tr>";
		$('#unrecordedTemp').append(rowData);
	}
	
}

function prepValue(value){
	
	var browserName=navigator.userAgent;
	var isIE=browserName.match(/MSIE/);
	
	  if(isIE)
      {
		  value = value.text;
      
      }
      else
      {
    	  value = value.textContent;
      	
      }
	return value;
}

function readData(dataBack, node, nodeData){
	
	var xmlDoc = $.parseXML(dataBack);
	var	 node = xmlDoc.getElementsByTagName(node)[0]; 
	var data = node.getElementsByTagName(nodeData)[0];
	return data;
}
